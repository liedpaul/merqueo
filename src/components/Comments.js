import React, { Component } from 'react';
import './comments.scss';
import avatar from'../avatar.jpg'


class Comments extends Component {

  constructor(props){
    super(props);
    this.state ={data:[]};
    this.handleClick = this.handleClick.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.name=props.name;
    this.desc="";
  }


  handleClick(e){
    e.preventDefault();
    this.state.data.push(this.desc);
    this.setState(
      this.state
    )
  }
  handleChange(e) {
    this.desc=e.target.value;
  }

  
  render() {
    return (
      <div>

        <div className="Comments">

        <ul>
        {
          this.state.data.map((item) => (
            <li className="item">
              <div className="avatar">
                <img src={avatar} className="image" />
              </div> 
              <div className="content">
                <p className="desc"> <strong className="title">{this.name}</strong> 
                
                {item}
                </p>
                <strong className="date">Hace un minuto</strong>
              </div>
          </li>
        ))
        }
        
        </ul>
          
          <form>
            <textarea placeholder="Escribe un comentario" onChange={this.handleChange}></textarea>
            <div className="control">
              <button onClick={this.handleClick}>Comentar</button>
            </div>
            
          </form>
          
        </div>

        </div>
    );
  }
}

export default Comments;
