import React, { Component } from 'react';
import './compose.scss';
import Posts from './Posts';

class Compose extends Component {
  constructor(props){
    super(props);
    this.state ={data:props.postdata};
    this.name=props.name;
    this.desc="...";
    this.handleChange = this.handleChange.bind(this);
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(e){
    e.preventDefault();
    this.state.data.push({nombre:this.name, desc:this.desc, comentarios:[]})
    this.setState(
      this.state
    )
    console.log("commn,,", this.state.data)
  }

  handleChange(e) {
    this.desc=e.target.value;
  }

  render() {
    
    return (
      <div>
        <div className="Compose">
          <form>
            <textarea placeholder="Escribe aquí tu estado" onChange={this.handleChange}></textarea>
            <div className="control">
              <button className="button" onClick={this.handleClick}>Publicar</button>
            </div>
            
          </form>
          
        </div>
      {
        this.state.data.map((item) => (
          <Posts postdata={item} name={this.name} />
        ))
      }
      </div>
    );
  }
}

export default Compose;

