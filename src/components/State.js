import React, { Component } from "react";
class ExampleComponent extends Component {
  constructor() {
    super();
    this.state = {
      articles: [
        { name: "Test1", id: 1, date:Date(), desc: "Redux e React: cos'è Redux e come usarlo con React"},
        { name: "Test2", id: 1, date:Date(), desc: "Redux e React: cos'è Redux e come usarlo con React"},
      ]
    };
  }
  render() {
    const { articles } = this.state;
    return <ul>{articles.map(el => <li key={el.id}>{el.title}</li>)}</ul>;
  }
}