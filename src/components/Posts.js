import React, { Component } from 'react';
import './posts.scss';
import Comments from './Comments';
import avatar from'../avatar.jpg'

class Posts extends Component {


  constructor(props){
    super(props);
    this.state ={data:props.postdata};
    this.handleClick = this.handleClick.bind(this);
    this.name=props.name;
    this.desc=props.postdata.desc;
    this.count=0;
    this.isComment=false;
  }


  handleClick(e){
    
    e.preventDefault();

    if(e.target.className==="secundary-button one"){
      this.count++;
    }else{
      this.isComment=true;
    }
    
    this.setState(
      this.state
    )

  }

  render() {

    let comentario;

    if(this.isComment){
      comentario=<Comments name={this.name} />;
    }


    return (
      <div className="Posts">
        <div className="avatar">
          <img src={avatar} className="image" />
        </div> 
        <div className="content">
          <strong className="title">{this.name}</strong>
          <strong className="date">Hace un minuto</strong>
          <p className="desc">{this.desc}</p>
          <span className="secundary-button one" onClick={this.handleClick}>Reaccionar</span>
          <span className="secundary-button two" onClick={this.handleClick}>Comentar</span>
        </div>

      <div className="footer">
          <div className="controls">
            <span className="blue"></span>
            <span className="red"></span>
            <span className="yellow"></span>
            <span className="count">{this.count}</span>
          </div>

          <p className="comments">{this.state.length} comentarios</p>
      </div>
          
      {comentario}

      </div>

);
  }
}

export default Posts;
