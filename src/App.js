import React, { Component } from 'react';
import './App.scss';
import Compose from './components/Compose';

class App extends Component {
  constructor() {
    super();
    this.state = {
      postdata : [{nombre:"", desc:"Esta es una publicación de prueba.", comentarios:[]}],
      name: "Juan Rodríguez"
    };
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(e){
    e.preventDefault();
    console.log("click home", this.state.postdata)
    this.setState (this.state.postdata);
    /*this.state.data.push({nombre:this.name, desc:this.desc})
    this.setState(
      this.state
    )*/
    
  }
  componentDidMount () {
     console.log("here.", this.state.postdata)
      this.setState (this.state.postdata);
  }

  render() {
    
    return (
      <div className="App">
        <header className="App-header">
          <h1 onClick={this.handleClick}>Domicilios Test</h1>
        </header>
        <Compose postdata={this.state.postdata} name={this.state.name} />
       

        
      </div>
    );
  }
}

export default App;
